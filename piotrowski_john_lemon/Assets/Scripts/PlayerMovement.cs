﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //USED CAMel case bc its public... 
    public float turnSpeed = 20f;
    public bool have_key = false;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;


    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis
            ("Horizontal");
        float vertical = Input.GetAxis
            ("Vertical");

        //set the movement vector 3 to the horizontal and vertical axes
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        //determine if there is horizontal input.
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        //determine if there is vertical input:
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        //is john lemons walking?
        bool isWalking = hasHorizontalInput ||
            hasVerticalInput;

        //setting animator
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward =
            Vector3.RotateTowards
            (transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    //declare new method to apply root motion:
    private void OnAnimatorMove()
    {
        //referencing rigidbody to call moveposition and pass in new position. -
        m_Rigidbody.MovePosition
            (m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    //when he hits the shrinky lixer:
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Elixer"))
        {
            transform.localScale = Vector3.one * .5f; //shrinks upon pickup
            Invoke("ReturnToFullSize", 13f); //this makes it go back to normal size

            Destroy(other.gameObject); //git rid of elixer when used
        }
        
        if (other.gameObject.CompareTag("Key"))//if he hits the key,
        {
            have_key = true; //set havekey to true

            Destroy(other.gameObject); //git rid of key item
        }
        //this one checks if the player has the key and the box collider is triggered.
        if (other.gameObject.CompareTag("DisWall"))
        {
            if (have_key == true)
            {
                Destroy(other.gameObject);
            }
            
        }
        
        if (other.gameObject.CompareTag("Heart"))
        {
            Destroy(other.gameObject);
            var enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in enemyArray)
            {
                Destroy(enemy);
            }
            
        }
    }

    void ReturnToFullSize()
    {
        transform.localScale = Vector3.one; //this allows him to become big again
    }
}
